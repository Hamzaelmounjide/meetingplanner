package com.helmounjide.meetingplanner.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.helmounjide.meetingplanner.dao.EquipementRepository;
import com.helmounjide.meetingplanner.dao.SalleRepository;
import com.helmounjide.meetingplanner.models.Salle;

@Service
@Transactional
public class SalleImplService implements SalleService{
	
	@Autowired private SalleRepository salleRepository;
	@Autowired private EquipementRepository equipementRepository;
	

	public void addEquipmentToSalle(String nameSalle) {
		Salle salle = salleRepository.findByName(nameSalle);
		if(salle.getReunion().getTypeReunion().equals("VC")) {
			salle.getEquipement().add(equipementRepository.findByNameEquipement("Pieuvre"));
			salle.getEquipement().add(equipementRepository.findByNameEquipement("Ecran"));
			salle.getEquipement().add(equipementRepository.findByNameEquipement("WebCam"));
		}else if(salle.getReunion().getTypeReunion().equals("SPEC")) {
			salle.getEquipement().add(equipementRepository.findByNameEquipement("Tableau"));
		}else if(salle.getReunion().getTypeReunion().equals("RC")){
			salle.getEquipement().add(equipementRepository.findByNameEquipement("Tableau"));
			salle.getEquipement().add(equipementRepository.findByNameEquipement("Ecran"));
			salle.getEquipement().add(equipementRepository.findByNameEquipement("Pieuvre"));
		}else {
			throw new RuntimeException("Le type de la réunion ça n'existe pas ou vous n'avez pas besoin d'équimenets ! ");
		}		
	}
	
	public Salle saveSalle(Long idSalle, String nameSalle) {
		Salle salle = salleRepository.findById(idSalle).get();
		if(salle != null) throw new RuntimeException("Salle déja existé !");
		Salle salle1 = new Salle();
		salle1.setName(nameSalle);
		addEquipmentToSalle(nameSalle);
		salleRepository.save(salle1);
		return salle1;
	}
	
	public List getAllSalles() {
		List salles = new ArrayList<Salle>();
		salleRepository.findAll().forEach(salles::add);
		return salles;
	}
	
	public void deleteSalle(Long idSalle) {
		salleRepository.deleteById(idSalle);
	}

}
