package com.helmounjide.meetingplanner.service;


import java.util.List;

import com.helmounjide.meetingplanner.models.Reunion;
import com.helmounjide.meetingplanner.models.Reunion.ReunionType;

public interface ReunionService {
	public Reunion saveReunion(ReunionType typeReunion, Long idSalle, Long idPerson);
	public void addPersonToReunion(ReunionType typeReunion, Long idPerson);
	public List getAllReunions();
	public void deleteReunion(Long idReunion);
}
