package com.helmounjide.meetingplanner.service;

import java.util.List;

import com.helmounjide.meetingplanner.models.Salle;

public interface SalleService {

	public void addEquipmentToSalle(String nameSalle);
	public Salle saveSalle(Long idSalle, String nameSalle);
	public List getAllSalles();
	public void deleteSalle(Long idSalle);
}
