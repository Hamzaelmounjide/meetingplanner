package com.helmounjide.meetingplanner.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.helmounjide.meetingplanner.dao.PersonRepository;
import com.helmounjide.meetingplanner.dao.ReunionRepository;
import com.helmounjide.meetingplanner.dao.SalleRepository;
import com.helmounjide.meetingplanner.models.Person;
import com.helmounjide.meetingplanner.models.Reunion;
import com.helmounjide.meetingplanner.models.Salle;
import com.helmounjide.meetingplanner.models.Reunion.ReunionType;

@Service
@Transactional
public class ReunionImplService implements ReunionService {

	@Autowired private SalleRepository salleRepository;
	@Autowired private ReunionRepository reunionRepository;
	@Autowired private PersonRepository personRepository;
	
	public Reunion saveReunion(ReunionType typeReunion, Long idSalle, Long idPerson) {
		LocalDateTime slotTimeStart = LocalDateTime.now();
		Reunion reunion = reunionRepository.findByTypeReunion(typeReunion);
		Salle salle = salleRepository.findById(idSalle).get();
		if(reunion != null) throw new RuntimeException("Reunion déja existé !");
		Reunion reunion1 = new Reunion();
		reunion1.setTypeReunion(typeReunion);
		reunion1.setSlotTimeStart(slotTimeStart);
		reunion1.setSlotTimeStop(slotTimeStart.plusHours(1));
		reunion1.setSalle(salle);
		addPersonToReunion(typeReunion, idPerson);
		reunionRepository.save(reunion1);
		return reunion1;
	}

	public void addPersonToReunion(ReunionType typeReunion, Long idPerson) {
		Reunion reunion = reunionRepository.findByTypeReunion(typeReunion);
		Person person = personRepository.findById(idPerson).get();
		if(reunion.getPerson().size() < reunion.getSalle().getCapacite()) {
			reunion.getPerson().add(person);
		}else {
			throw new RuntimeException("vous avez atteint le nombre maximum de personnes");
		}
	}
	
	public void deleteReunion(Long idReunion) {
		reunionRepository.deleteById(idReunion);
	}
	
	public List getAllReunions() {
		List reunions = new ArrayList<Reunion>();
		reunionRepository.findAll().forEach(reunions::add);
		return reunions;
	}
	

}
