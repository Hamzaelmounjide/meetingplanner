package com.helmounjide.meetingplanner.models;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="reunion")
public class Reunion {
	
	public enum ReunionType {VC, SPEC, RS, RC};
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "salle_id", referencedColumnName = "id", nullable=false)
    private Salle salle;
	
	@Enumerated(EnumType.STRING)
	@Column(name="typeReunion")
	private ReunionType typeReunion;
	
	@Column(name = "slotTimeStart")
	private LocalDateTime slotTimeStart;
	
	@Column(name = "slotTimeStop")
	private LocalDateTime slotTimeStop;
		
	@OneToMany(targetEntity=Person.class, mappedBy="reunion_person",cascade=CascadeType.ALL, fetch = FetchType.LAZY)    
	private List<Person> person = new ArrayList<Person>();

	public Reunion() {
		super();
	}
	
	public Reunion(long id, Salle salle, ReunionType typeReunion, LocalDateTime slotTimeStart,
			LocalDateTime slotTimeStop, List<Person> person) {
		super();
		this.id = id;
		this.salle = salle;
		this.typeReunion = typeReunion;
		this.slotTimeStart = slotTimeStart;
		this.slotTimeStop = slotTimeStop;
		this.person = person;
	}

	public ReunionType getTypeReunion() {
		return typeReunion;
	}

	public void setTypeReunion(ReunionType typeReunion) {
		this.typeReunion = typeReunion;
	}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}

	public Salle getSalle() {
		return salle;
	}


	public void setSalle(Salle salle) {
		this.salle = salle;
	}


	public LocalDateTime getSlotTimeStart() {
		return slotTimeStart;
	}


	public void setSlotTimeStart(LocalDateTime slotTimeStart) {
		this.slotTimeStart = slotTimeStart;
	}


	public LocalDateTime getSlotTimeStop() {
		return slotTimeStop;
	}


	public void setSlotTimeStop(LocalDateTime slotTimeStop) {
		this.slotTimeStop = slotTimeStop;
	}


	public List<Person> getPerson() {
		return person;
	}

	public void setPerson(List<Person> person) {
		this.person = person;
	}
}
