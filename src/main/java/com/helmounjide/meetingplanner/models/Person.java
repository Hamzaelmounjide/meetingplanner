package com.helmounjide.meetingplanner.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="person")
public class Person {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long person_id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="reunion_id")    
	private Reunion reunion_person;
	
	public Person() {
		super();
	}

	public Person(long person_id, Reunion reunion_person) {
		super();
		this.person_id = person_id;
		this.reunion_person = reunion_person;
	}

	public long getPerson_id() {
		return person_id;
	}

	public void setPerson_id(long person_id) {
		this.person_id = person_id;
	}

	public Reunion getReunion_person() {
		return reunion_person;
	}

	public void setReunion_person(Reunion reunion_person) {
		this.reunion_person = reunion_person;
	}
}
