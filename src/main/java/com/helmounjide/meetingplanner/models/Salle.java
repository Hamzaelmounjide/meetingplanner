package com.helmounjide.meetingplanner.models;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="salle")
public class Salle {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "name_salle", nullable = false)
	private String name;
	
	@Column(name = "capacite")
	private int capacite;

	@OneToMany(mappedBy="salle_equipement", cascade=CascadeType.ALL)
	private List<Equipement> equipement = new ArrayList<Equipement>();
	
    @OneToOne(mappedBy="salle")
    private Reunion reunion;
	
	public Salle() {
		super();
	}

	public Salle(long id, String name, int capacite, List<Equipement> equipement, Reunion reunion) {
		super();
		this.id = id;
		this.name = name;
		this.capacite = capacite;
		this.equipement = equipement;
		this.reunion = reunion;
	}

	public Reunion getReunion() {
		return reunion;
	}

	public void setReunion(Reunion reunion) {
		this.reunion = reunion;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getCapacite() {
		return (capacite*70)/100;
	}

	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Equipement> getEquipement() {
		return equipement;
	}

	public void setEquipement(List<Equipement> equipement) {
		this.equipement = equipement;
	}	
}
