package com.helmounjide.meetingplanner.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="equipement")
public class Equipement {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="nameEquipement")
	private String nameEquipement;
	
	@ManyToOne
	@JoinColumn(name="salle_id")
	private Salle salle_equipement;
	
	public Equipement(long id, String nameEquipement, Salle salle_equipement) {
		super();
		this.id = id;
		this.nameEquipement = nameEquipement;
		this.salle_equipement = salle_equipement;
	}

	public Equipement() {
		super();
	}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}

	public Salle getSalle_equipement() {
		return salle_equipement;
	}

	public void setSalle_equipement(Salle salle_equipement) {
		this.salle_equipement = salle_equipement;
	}

	public String getNameEquipement() {
		return nameEquipement;
	}

	public void setNameEquipement(String nameEquipement) {
		this.nameEquipement = nameEquipement;
	}

	public Salle getSalle() {
		return salle_equipement;
	}

	public void setSalle(Salle salle_equipement) {
		this.salle_equipement = salle_equipement;
	}
}
