package com.helmounjide.meetingplanner;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.helmounjide.meetingplanner.dao.EquipementRepository;
import com.helmounjide.meetingplanner.dao.ReunionRepository;
import com.helmounjide.meetingplanner.models.Equipement;
import com.helmounjide.meetingplanner.models.Reunion;
import com.helmounjide.meetingplanner.models.Reunion.ReunionType;
import com.helmounjide.meetingplanner.models.Salle;

@SpringBootApplication
public class MeetingplannerApplication implements CommandLineRunner{
	
	@Autowired private ReunionRepository reunionRepository;
	@Autowired private EquipementRepository equipementRepository;

	public static void main(String[] args) {
		SpringApplication.run(MeetingplannerApplication.class, args);
	}

	public void run(String... args) throws Exception {
		/*Ajout test salle*/
		Salle salle2 = new Salle();
		salle2.setName("E002");
		salle2.setCapacite(16);
		
		/*Ajout test reunion*/
		Reunion reunion = new Reunion();
		reunion.setTypeReunion(ReunionType.VC);
		reunion.setSlotTimeStart(LocalDateTime.parse("2021-09-23T08:00:00"));
		reunion.setSlotTimeStop(LocalDateTime.parse("2021-09-23T09:00:00"));
		reunion.setSalle(salle2);
		
		reunionRepository.save(reunion);
		
		/*Ajout test equipement*/
		Equipement equipement = new Equipement();
		equipement.setNameEquipement("Ecran");
		equipement.setSalle(salle2);
		
		equipementRepository.save(equipement);
	}
}
