package com.helmounjide.meetingplanner.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.helmounjide.meetingplanner.models.Reunion;
import com.helmounjide.meetingplanner.models.Reunion.ReunionType;
import com.helmounjide.meetingplanner.models.Salle;
import com.helmounjide.meetingplanner.service.ReunionService;
import com.helmounjide.meetingplanner.service.SalleService;

@RestController
public class MainController {
	@Autowired private SalleService salleService;
	@Autowired private ReunionService reunionService;
	
	
	@PostMapping("/registerReunion")
	public Reunion registerReunion(@RequestParam ReunionType typeReunion,
								   @RequestParam Long idSalle, 
								   @RequestParam Long idPerson) {
		return reunionService.saveReunion(typeReunion, idSalle, idPerson);
	}
	
	@PostMapping("/registerSalle")
	public Salle registerSalle(@RequestParam String nameSalle,
								   @RequestParam Long idSalle) {
		return salleService.saveSalle(idSalle, nameSalle);
	}
	
	@RequestMapping(value = "/salles", method = RequestMethod.GET)
	public List getAllSalles() {
		return salleService.getAllSalles();
	}
	
	@RequestMapping(value = "/reunions", method = RequestMethod.GET)
	public List getAllReunions() {
		return reunionService.getAllReunions();
	}
	
	@RequestMapping(value = "/reunions/{id}", method = RequestMethod.DELETE)
	public void deleteReunion(@PathVariable Long id) {
		reunionService.deleteReunion(id);
	}
	
	@RequestMapping(value = "/salles/{id}", method = RequestMethod.DELETE)
	public void deleteSalle(@PathVariable Long id) {
		salleService.deleteSalle(id);
	}
	
}
