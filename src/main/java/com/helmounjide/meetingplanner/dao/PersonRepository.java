package com.helmounjide.meetingplanner.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.helmounjide.meetingplanner.models.Person;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {}
