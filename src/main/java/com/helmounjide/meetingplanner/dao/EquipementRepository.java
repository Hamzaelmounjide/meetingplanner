package com.helmounjide.meetingplanner.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.helmounjide.meetingplanner.models.Equipement;

@Repository
public interface EquipementRepository extends CrudRepository<Equipement, Long> {
	public Equipement findByNameEquipement(String nameEquipement);
}
