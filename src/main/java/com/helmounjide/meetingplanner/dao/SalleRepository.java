package com.helmounjide.meetingplanner.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.helmounjide.meetingplanner.models.Salle;

@Repository
public interface SalleRepository extends CrudRepository<Salle, Long>{
	
	public Salle findByName(String nameSalle);
}
