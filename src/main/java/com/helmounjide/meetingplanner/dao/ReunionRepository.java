package com.helmounjide.meetingplanner.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.helmounjide.meetingplanner.models.Reunion;
import com.helmounjide.meetingplanner.models.Reunion.ReunionType;

@Repository
public interface ReunionRepository extends CrudRepository<Reunion,Long> {
	public Reunion findByTypeReunion(ReunionType typeReunion);
}
